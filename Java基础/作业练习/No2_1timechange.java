package javahomework;

import java.util.Scanner;

public class No2_1timechange {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("将 北京时间BJT 转换为 世界协调时 UTC");
		System.out.println("请输入4位北京时间（0000-2359）：");
		
		Scanner in = new Scanner(System.in);
		int BJTtime = in.nextInt();
		
		//如果不输入整数会报错，暂时不考虑输入非整数的异常处理，然后先排除负数。
		if(BJTtime < 0)
		{
			System.out.println("请输入正整数！");
			System.exit(0);
		}
		
		//这步只是判断是否是4位以下整数，不能排除如1299的异常
		if(BJTtime > 2359)
		{
			System.out.println("请输入4位及以下时间整数！");
			System.exit(0);
		}
		
		//小时须不大于23，分钟须不大于59
		int Hourtime = BJTtime / 100;
		int Minutetime = BJTtime % 100;
		if(Hourtime > 23 || Minutetime > 59)
		{
			System.out.println("小时须不大于23，分钟须不大于59！");
			System.exit(0);
		}
		else
		{
			System.out.println("BJT：" + Hourtime + "时" + Minutetime +"分");
		}
		
		//开始转换
		
		int UTCtime;
		int UTCHourtime;
		
		if(Hourtime > 7)
		{
			UTCHourtime = Hourtime - 8;
		}
		else
		{
			UTCHourtime = Hourtime - 8 + 24;
		}
		
		//暂不考虑0的空位不输出的情况
		UTCtime = UTCHourtime * 100 + Minutetime;
		System.out.println("UTC：" + UTCHourtime + "时" + Minutetime +"分");
		System.out.println(UTCtime);

	}

}
